DROP DATABASE IF EXISTS greta;

CREATE DATABASE greta CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE greta;

GRANT ALL PRIVILEGES ON greta.* TO 'John'@'localhost';

CREATE TABLE `type_flux` (
    `id_flux` INT AUTO_INCREMENT PRIMARY KEY,
    `type` VARCHAR(255) NOT NULL
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `sources` (
    `id_sources` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `id_flux` INT NOT NULL DEFAULT 1,
    FOREIGN KEY (`id_flux`) REFERENCES `type_flux`(`id_flux`)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `category` (
    `id_category` INT AUTO_INCREMENT PRIMARY KEY,
    `name_categ` VARCHAR(255) NOT NULL
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `news` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `URL_news` VARCHAR(255) NOT NULL,
    `date_publication` DATE NOT NULL,
    `id_sources` INT NOT NULL,
    FOREIGN KEY (`id_sources`) REFERENCES `sources`(`id_sources`)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `news_category` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `id_news` INT NOT NULL,
    `id_category` INT NOT NULL,
    FOREIGN KEY (`id_news`) REFERENCES `news`(`id`),
    FOREIGN KEY (`id_category`) REFERENCES `category`(`id_category`)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;