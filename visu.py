import dash
from dash import dcc, html, dash_table
from dash.dependencies import Input, Output, State
import requests

# Initialisation de l'application Dash
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

# Layout principal avec des onglets pour la navigation
app.layout = html.Div([
    dcc.Tabs(id="tabs", value='tab-1', children=[
        dcc.Tab(label='Articles par mots-clés et ID', value='tab-1'),
        dcc.Tab(label='Articles par Catégories', value='tab-2'),
        dcc.Tab(label='Articles par Sources', value='tab-3'),
        dcc.Tab(label='Articles: Option de tri par Date', value='tab-4'),
    ]),
    html.Div(id='tabs-content')
])

# Fonction pour récupérer les données de l'API
def get_api_data():
    try:
        response = requests.get("http://127.0.0.1:8000/articles")
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return []

def get_article_by_id(article_id):
    try:
        response = requests.get(f"http://127.0.0.1:8000/articles/{article_id}")
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching article by ID from API: {e}")
        return None

def get_articles_by_keywords(keywords):
    try:
        if keywords:
            keywords_str = ",".join(keywords)
            response = requests.get(f"http://127.0.0.1:8000/articles/keywords?keywords={keywords_str}")
        else:
            response = requests.get("http://127.0.0.1:8000/articles/keywords")
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching articles by keywords from API: {e}")
        return []

def get_articles_by_category(category):
    try:
        response = requests.get(f"http://127.0.0.1:8000/articles/category", params={"category": category})
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching articles by category from API: {e}")
        return []

def get_categories():
    try:
        response = requests.get("http://127.0.0.1:8000/categories")
        response.raise_for_status()
        data = response.json()
        return data['categories']
    except requests.RequestException as e:
        print(f"Error fetching categories from API: {e}")
        return []

def get_sources():
    try:
        response = requests.get("http://127.0.0.1:8000/sources")
        response.raise_for_status()
        data = response.json()
        return data['sources']
    except requests.RequestException as e:
        print(f"Error fetching sources from API: {e}")
        return []

def get_articles_by_source(source_id):
    try:
        response = requests.get(f"http://127.0.0.1:8000/articles/source/{source_id}")
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching articles by source from API: {e}")
        return []

def get_articles_with_filters(params):
    try:
        response = requests.get("http://127.0.0.1:8000/articles", params=params)
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching articles with filters from API: {e}")
        return []

# Layout pour l'application 1
tab_1_layout = html.Div([
    html.H1("Articles"),
    dcc.Input(id='article-id-input', type='number', placeholder="Entrez l'ID de l'article"),
    html.Button('Rechercher', id='article-search-button', n_clicks=0),
    dcc.Input(id='keywords-input', type='text', placeholder='Entrez les mots-clés'),
    html.Button('Rechercher par mots-clés', id='search-button', n_clicks=0),
    dcc.Interval(id='interval-component', interval=60*1000, n_intervals=0),
    dash_table.DataTable(
        id='table',
        columns=[
            {'name': 'ID', 'id': 'id'},
            {'name': 'Title', 'id': 'title'},
            {'name': 'Description', 'id': 'description'},
            {'name': 'URL', 'id': 'URL_news'},
            {'name': 'Publication Date', 'id': 'date_publication'},
            {'name': 'Source ID', 'id': 'id_sources'}
        ],
        data=[],
        page_current=0,
        page_size=10,
        page_action='custom',
        style_table={'width': '100%'},
        style_cell={
            'textAlign': 'left',
            'padding': '10px',
            'font-size': '13px'
        },
        style_cell_conditional=[
            {'if': {'column_id': 'id'}, 'width': '5%'},
            {'if': {'column_id': 'title'}, 'width': '20%'},
            {'if': {'column_id': 'description'}, 'width': '40%'},
            {'if': {'column_id': 'URL_news'}, 'width': '20%'},
            {'if': {'column_id': 'date_publication'}, 'width': '10%'},
            {'if': {'column_id': 'id_sources'}, 'width': '5%'}
        ],
        style_header={
            'backgroundColor': '#545E56',
            'color': 'white',
            'textAlign': 'center',
            'fontWeight': 'bold'
        }
    ),
    html.Div(id='article-details')
])

# Layout pour l'application 2
tab_2_layout = html.Div([
    html.H1("Articles par catégorie"),
    dcc.Dropdown(id='category-dropdown', placeholder='Sélectionnez une catégorie'),
    html.Div(id='category-articles')
])

# Layout pour l'application 3
tab_3_layout = html.Div([
    html.H1("Articles par Source"),
    dcc.Dropdown(id='source-dropdown', placeholder='Sélectionnez une source'),
    html.Div(id='source-articles')
])


# Layout pour l'application 4
tab_4_layout = html.Div([
    html.H1("Articles avec option de tri par date"),
    dcc.DatePickerRange(
        id='date-picker-range',
        display_format='YYYY-MM-DD',
        start_date_placeholder_text='Date de début',
        end_date_placeholder_text='Date de fin'
    ),
    dcc.Dropdown(
        id='sort-by-date-dropdown',
        options=[
            {'label': 'Croissant', 'value': 'asc'},
            {'label': 'Décroissant', 'value': 'desc'}
        ],
        placeholder='Trier par date'
    ),
    html.Button('Rechercher', id='search-button-4', n_clicks=0),
    html.Div(id='articles-list')
])

# Callback pour afficher le contenu en fonction de l'onglet sélectionné
@app.callback(Output('tabs-content', 'children'), [Input('tabs', 'value')])
def render_content(tab):
    if (tab == 'tab-1'):
        return tab_1_layout
    elif (tab == 'tab-2'):
        return tab_2_layout
    elif (tab == 'tab-3'):
        return tab_3_layout
    elif (tab == 'tab-4'):
        return tab_4_layout

# Callbacks pour l'application 1
@app.callback(
    Output('table', 'data'),
    [Input('search-button', 'n_clicks'), Input('table', 'page_current'), Input('table', 'page_size')],
    [State('keywords-input', 'value')]
)
def update_data(n_clicks, page_current, page_size, keywords):
    if keywords is not None:
        articles = get_articles_by_keywords(keywords.split())
    else:
        articles = get_api_data()
    start = page_current * page_size
    end = start + page_size
    for article in articles:
        # article['id'] = html.A(f"{article['id']}", href=f"#{article['id']}")
         article['id'] = str(article['id'])  # Convertir l'ID en string
    return articles[start:end]

@app.callback(
    Output('article-details', 'children'),
    [Input('article-search-button', 'n_clicks'), Input('table', 'active_cell')],
    [State('article-id-input', 'value')]
)
def update_article_details(n_clicks, active_cell, article_id):
    ctx = dash.callback_context
    triggered_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if triggered_id == 'article-search-button' and n_clicks > 0 and article_id is not None:
        article_details = get_article_by_id(article_id)
    elif triggered_id == 'table' and active_cell:
        article_id = active_cell['row_id']
        article_details = get_article_by_id(article_id)
    else:
        return html.P("Cliquez sur un ID d'article dans le tableau ou saisissez un ID et cliquez sur 'Rechercher'.")

    if article_details:
        return html.Div([
            html.H2(article_details['title']),
            html.P(article_details['description']),
            html.A('Lien vers l\'article', href=article_details['URL_news']),
            html.P(f"Date de publication : {article_details['date_publication']}"),
            html.P(f"Source : {article_details['source']}"),
            html.P(f"Catégories : {', '.join(article_details['categories'])}")
        ])
    else:
        return html.P("Aucun article trouvé pour cet ID.")

# Callbacks pour l'application 2
@app.callback(
    Output('category-dropdown', 'options'),
    [Input('tabs', 'value')]
)
def update_category_dropdown(tab):
    if tab == 'tab-2':
        categories = get_categories()
        return [{'label': category, 'value': category} for category in categories]
    else:
        return []

@app.callback(
    Output('category-articles', 'children'),
    [Input('category-dropdown', 'value')]
)
def update_category_articles(category):
    if category:
        articles = get_articles_by_category(category)
        if articles:
            articles_div = [
                html.Div([
                    html.H3(article['title']),
                    html.P(article['description']),
                    html.A('Lien vers l\'article', href=article['URL_news']),
                    html.P(f"Date de publication : {article['date_publication']}"),
                    html.P(f"Source : {article['source_name']}")
                ])
                for article in articles['articles']
            ]
            return html.Div(articles_div)
        else:
            return html.P("Aucun article trouvé pour cette catégorie.")
    else:
        return html.P("Sélectionnez une catégorie.")

# Callbacks pour l'application 3
@app.callback(
    Output('source-dropdown', 'options'),
    [Input('tabs', 'value')]
)
def update_source_dropdown(tab):
    if tab == 'tab-3':
        sources = get_sources()
        return [{'label': source['name'], 'value': source['id_sources']} for source in sources]
    else:
        return []

@app.callback(
    Output('source-articles', 'children'),
    [Input('source-dropdown', 'value')]
)
def update_source_articles(source_id):
    if source_id:
        articles = get_articles_by_source(source_id)
        if articles:
            articles_div = [
                html.Div([
                    html.H3(article['title']),
                    html.P(article['description']),
                    html.A('Lien vers l\'article', href=article['URL_news']),
                    html.P(f"Date de publication : {article['date_publication']}")
                ])
                for article in articles
            ]
            return html.Div(articles_div)
        else:
            return html.P("Aucun article trouvé pour cette source.")
    else:
        return html.P("Sélectionnez une source.")

# Callbacks pour l'application 4
@app.callback(
    Output('articles-list', 'children'),
    [Input('search-button-4', 'n_clicks')],
    [State('date-picker-range', 'start_date'), State('date-picker-range', 'end_date'), State('sort-by-date-dropdown', 'value')]
)
def update_articles_list(n_clicks, start_date, end_date, sort_by_date):
    if n_clicks > 0:
        params = {}
        if start_date:
            params['start_date'] = start_date
        if end_date:
            params['end_date'] = end_date
        if sort_by_date:
            params['sort_by_date'] = sort_by_date
        articles = get_articles_with_filters(params)
        if articles:
            articles_div = [
                html.Div([
                    html.H3(article['title']),
                    html.P(article['description']),
                    html.A('Lien vers l\'article', href=article['URL_news']),
                    html.P(f"Date de publication : {article['date_publication']}"),
                    html.P(f"Source : {article['id_sources']}")
                ])
                for article in articles
            ]
            return html.Div(articles_div)
        else:
            return html.P("Aucun article trouvé avec les filtres sélectionnés.")
    else:
        return html.P("Sélectionnez des filtres et cliquez sur 'Rechercher'.")

if __name__ == '__main__':
    app.run_server(debug=True)