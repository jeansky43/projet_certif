#!/bin/bash

# Step 1: Create a virtual environment
python3 -m venv venv
source venv/bin/activate

# Step 2: Install required Python packages
pip install -r requirements.txt

# Step 3: Export environment variables for sensitive data
export DB_HOST="localhost"
export DB_USER="John"
export DB_PASSWORD="docu"
export DB_NAME="greta"

# Step 4: Initialize the database
mysql -u $DB_USER -p$DB_PASSWORD < init.sql

echo "Setup complete. Remember to source the virtual environment using 'source venv/bin/activate' before running your scripts."

# Note: To deactivate the virtual environment, run 'deactivate'
