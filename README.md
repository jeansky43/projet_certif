# Agrégateur de Nouvelles

Ce projet fournit une solution complète pour extraire des données, configurer une base de données, créer une API et visualiser les données en utilisant Dash. Il utilise Python et plusieurs bibliothèques pour garantir une utilisation et un déploiement faciles.

L'Agrégateur de Nouvelles est une application complète qui collecte, stocke et affiche des articles provenant de différentes sources RSS. Ce projet propose une API pour interagir avec les données et une interface utilisateur pour visualiser les nouvelles et des graphiques.


## Table des Matières

- [Vue d'ensemble du projet](#vue-densemble-du-projet)
- [Fonctionnalités](#fonctionnalités)
- [Prérequis](#prérequis)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [Aperçu des scripts](#aperçu-des-scripts)
- [Contribution](#contribution)
- [Licence](#licence)

## Vue d'ensemble du projet

Ce projet comprend les composants suivants :
- **Extraction de données** : Extraction des données à partir de flux RSS et stockage dans une base de données MySQL.
- **API** : Fournit une API pour accéder aux données via FastAPI.
- **Visualisation des données** : Visualisation des données à l'aide de Dash et Plotly.

## Fonctionnalités

- Configuration et déploiement faciles avec un script bash.
- Gestion sécurisée des informations d'identification de la base de données via des variables d'environnement.
- Extraction et stockage complets des données.
- API basée sur FastAPI pour l'accès aux données.
- Visualisation interactive des données avec Dash et Plotly.

## Prérequis

- Python 3.8 ou supérieur
- MySQL
- Bash

## Installation

1. **Cloner le dépôt** :

    ```bash
    git clone https://gitlab.com/jeansky43/projet_certif
    cd agregateur-de-nouvelles
    ```

2. **Créer un fichier `requirements.txt`** (si ce n'est pas déjà fait) avec le contenu suivant :

    ```
    feedparser
    mysql-connector-python
    fastapi
    uvicorn
    pydantic
    dash
    plotly
    pandas
    requests
    ```

3. **Exécuter le script de configuration** :

    ```bash
    chmod +x setup.sh
    ./setup.sh
    ```

4. **Activer l'environnement virtuel** :

    ```bash
    source venv/bin/activate
    ```

## Utilisation

1. **Extraction des données** :

    ```bash
    python extract.py
    ```

2. **Lancer l'API** :

    ```bash
    fastAPI dev main.py
    ```

3. **Lancer l'interface utilisateur** :

    ```bash
    python visu.py
    ```

4. **Lancer la visualisation des graphiques** :

    ```bash
    python visu_chart.py
    ```

5. **Désactiver l'environnement virtuel** lorsque vous avez terminé :

    ```bash
    deactivate
    ```

## Aperçu des scripts

### `setup.sh`

- Crée un environnement virtuel
- Installe les packages Python requis à partir de `requirements.txt`
- Définit les variables d'environnement pour les informations d'identification de la base de données
- Initialise la base de données MySQL en utilisant `init.sql`

### `extract.py`

Extrait des données à partir de flux RSS et les stocke dans la base de données MySQL.

### `main.py`

Fournit une API utilisant FastAPI pour accéder aux données stockées dans la base de données.

### `models.py`

Définit les modèles de données utilisés par l'API.

### `visu.py`

Crée l'interface utilisateur en utilisant Dash.

### `visu_chart.py`

Crée les graphiques en utilisant Plotly.

## Contribution

Les contributions sont les bienvenues ! 

## Licence


