
from pydantic import BaseModel
from datetime import datetime


class NewsCategory(BaseModel):
    id: int
    id_news: int
    id_category: int

    class Config:
        orm_mode = True


class News(BaseModel):
    id: int
    title: str
    description: str
    URL_news: str
    date_publication: str
    id_sources: int

    class Config:
        orm_mode = True


class TypeFlux(BaseModel):
    id_flux: int
    type: str

    class Config:
        orm_mode = True


class Category(BaseModel):
    id_category: int
    name_categ: str

    class Config:
        orm_mode = True


class Source(BaseModel):
    id_sources: int
    name: str
    id_flux: int

    class Config:
        orm_mode = True

    