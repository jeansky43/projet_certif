import feedparser
import mysql.connector
from datetime import datetime

# Connexion à la base de données
connection = mysql.connector.connect(
    host="xxxx",
    user="xxxx",
    password="xxxx",
    database="xxxx"
)
cursor = connection.cursor()


# Fonction pour insérer les données dans la base de données
def insert_data(title, description, link, date_publication, creator, category):
    # Récupérer l'ID de la source à partir de la base de données
    cursor.execute("SELECT id_sources FROM sources WHERE name = %s", (creator,))
    source_row = cursor.fetchone()
    if source_row:
        source_id = source_row[0]
    else:
        # Si la source n'existe pas, l'ajouter à la base de données
        cursor.execute("INSERT INTO sources (name) VALUES (%s)", (creator,))
        connection.commit()
        source_id = cursor.lastrowid

    # Récupérer l'ID de la catégorie à partir de la base de données
    cursor.execute("SELECT id_category FROM category WHERE name_categ = %s", (category,))
    category_row = cursor.fetchone()
    if category_row:
        category_id = category_row[0]
    else:
        # Si la catégorie n'existe pas, l'ajouter à la base de données
        cursor.execute("INSERT INTO category (name_categ) VALUES (%s)", (category,))
        connection.commit()
        category_id = cursor.lastrowid

    # Insérer les données dans la table news
    cursor.execute('''INSERT INTO news (title, description, URL_news, date_publication, id_sources)
                      VALUES (%s, %s, %s, %s, %s)''',
                   (title, description, link, date_publication, source_id))
    news_id = cursor.lastrowid

    # Insérer les données dans la table news_category
    cursor.execute('''INSERT INTO news_category (id_news, id_category)
                      VALUES (%s, %s)''',
                   (news_id, category_id))

# Fonction pour insérer le sous-titre dans la table type_flux
def insert_subtitle(subtitle):
  
    # Vérifier si le sous-titre existe déjà dans la base de données
    cursor.execute("SELECT id_flux FROM type_flux WHERE type = %s", (subtitle,))
    subtitle_row = cursor.fetchone()
    if not subtitle_row:
        # Si le sous-titre n'existe pas, l'ajouter à la base de données
        cursor.execute("INSERT INTO type_flux (type) VALUES (%s)", (subtitle,))

    # Valider la transaction
    connection.commit()

# Fonction pour extraire et insérer les données à partir d'un flux RSS
def extract_and_insert_rss(url):
    # Parser le flux RSS
    feed = feedparser.parse(url)
    # Insérer le sous-titre dans la table type_flux
    insert_subtitle(feed.feed.get('subtitle', ''))

    # Parcourir les entrées du flux RSS
    for entry in feed.entries:
        title = entry.title
        description = entry.description
        link = entry.link
        creator = entry.get('author', '')
       # Convertir la date de publication
        if 'published' in entry:  # Vérifier si la clé 'published' est présente
            date_publication = datetime.strptime(entry.get('published', ''), "%a, %d %b %Y %H:%M:%S %z")
        elif 'dc:date' in entry:  # Si 'published' n'est pas présente, vérifier si la clé 'dc:date' est présente
            date_publication = datetime.strptime(entry.get('dc:date', ''), "%Y-%m-%dT%H:%M:%S")
        else:
            date_publication = datetime.now()  # Si aucune des deux clés n'est présente, utiliser l'heure actuelle
        
        # Récupérer la catégorie de l'article
        if 'category' in entry:
            category = entry.category
        else:
            category = 'No category'  # Si la catégorie n'est pas présente, utiliser une chaîne vide
        # Insérer les données dans la base de données
        insert_data(title, description, link, date_publication, creator, category)

# Insérer les URLs de chaque flux_RSS
rss_url = [
    {
        "url": "https://www.cnews.fr/rss/categorie/sport",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/sante",
    },

    {
        "url": "https://www.lemondeinformatique.fr/flux-rss/thematique/datacenter/rss.xml",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/culture",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/nutrition",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/vie-numerique",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/beaute",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/monde",
    },
   
    {
        "url": "https://www.cnews.fr/rss/categorie/france",
    },

    {
        "url": "https://www.cnews.fr/rss/categorie/faits%20divers",
    },

]

for rss in rss_url:
    extract_and_insert_rss(rss["url"])

# Fermeture du curseur et de la connexion
cursor.close()
connection.close()




