import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State
import requests
import plotly.express as px
import pandas as pd

# Initialisation de l'application Dash
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

# Fonction pour récupérer les données de l'API pour les catégories
def get_categories():
    try:
        response = requests.get("http://127.0.0.1:8000/categories")
        response.raise_for_status()
        data = response.json()
        return data['categories']
    except requests.RequestException as e:
        print(f"Error fetching categories from API: {e}")
        return []

def get_articles_by_category(category):
    try:
        response = requests.get(f"http://127.0.0.1:8000/articles/category", params={"category": category})
        response.raise_for_status()
        data = response.json()
        return data['articles']
    except requests.RequestException as e:
        print(f"Error fetching articles by category from API: {e}")
        return []

# Fonction pour obtenir le nombre d'articles par catégorie
def get_article_counts_by_category():
    categories = get_categories()
    category_counts = []
    for category in categories:
        articles = get_articles_by_category(category)
        category_counts.append({'category': category, 'count': len(articles)})
    return category_counts

# Fonction pour récupérer les données de l'API pour les sources
def get_sources():
    try:
        response = requests.get("http://127.0.0.1:8000/sources")
        response.raise_for_status()
        data = response.json()
        return data['sources']
    except requests.RequestException as e:
        print(f"Error fetching sources from API: {e}")
        return []

def get_articles():
    try:
        response = requests.get("http://127.0.0.1:8000/articles")
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching articles from API: {e}")
        return []

# Fonction pour obtenir le nombre d'articles par source
def get_article_counts_by_source():
    articles = get_articles()
    sources = get_sources()
    source_names = {source['id_sources']: source['name'] for source in sources}
    
    source_counts = {}
    for article in articles:
        source_id = article['id_sources']
        source_name = source_names.get(source_id, 'name')
        if source_name in source_counts:
            source_counts[source_name] += 1
        else:
            source_counts[source_name] = 1
    return [{'source': source, 'count': count} for source, count in source_counts.items()]

# Fonction pour filtrer les articles par date
def filter_articles_by_date(start_date, end_date):
    articles = get_articles()
    filtered_articles = [
        article for article in articles
        if start_date <= article['date_publication'] <= end_date
    ]
    return filtered_articles

# Layout principal avec des onglets
app.layout = html.Div([
    dcc.Tabs(id='tabs', value='tab-1', children=[
        dcc.Tab(label='Articles par Catégorie', value='tab-1'),
        dcc.Tab(label='Articles par Source', value='tab-2'),
        dcc.Tab(label='Articles par Date', value='tab-3')
    ]),
    html.Div(id='tabs-content')
])

# Callbacks pour afficher le contenu en fonction de l'onglet sélectionné
@app.callback(Output('tabs-content', 'children'), [Input('tabs', 'value')])
def render_content(tab):
    if tab == 'tab-1':
        category_counts = get_article_counts_by_category()
        fig = px.bar(
            category_counts,
            x='category',
            y='count',
            labels={'category': 'Catégorie', 'count': 'Nombre d\'articles'},
            title='Nombre d\'articles par catégorie'
        )
        return html.Div([
            html.H1("Nombre d'articles par catégorie"),
            dcc.Graph(id='category-articles-bar-chart', figure=fig)
        ])
    elif tab == 'tab-2':
        return html.Div([
            html.H1("Distribution des articles par source"),
            dcc.Dropdown(
                id='chart-type-dropdown',
                options=[
                    {'label': 'Pie Chart', 'value': 'pie'},
                    {'label': 'Bar Chart', 'value': 'bar'}
                ],
                value='pie',
                clearable=False
            ),
            dcc.Graph(id='source-articles-chart')
        ])
    elif tab == 'tab-3':
        return html.Div([
            html.H1("Évolution du nombre d'articles publiés"),
            dcc.DatePickerRange(
                id='date-picker-range',
                start_date='2024-01-01',
                end_date='2025-01-01',
                display_format='YYYY-MM-DD'
            ),
            html.Button('Rechercher', id='search-button', n_clicks=0),
            dcc.Graph(id='articles-line-chart')
        ])

# Callback pour mettre à jour le graphique par source
@app.callback(
    Output('source-articles-chart', 'figure'),
    [Input('chart-type-dropdown', 'value')]
)
def update_chart(chart_type):
    source_counts = get_article_counts_by_source()
    source_counts_sorted = sorted(source_counts, key=lambda x: x['source'])

    if chart_type == 'pie':
        fig = px.pie(
            source_counts_sorted,
            names='source',
            values='count',
            title='Distribution des articles par source'
        )
        fig.update_layout(
            autosize=False,
            width=1000,
            height=800,
        )
    elif chart_type == 'bar':
        fig = px.bar(
            source_counts_sorted,
            x='source',
            y='count',
            labels={'source': 'Source', 'count': 'Nombre d\'articles'},
            title='Distribution des articles par source'
        )
        fig.update_layout(
            autosize=False,
            width=1000,
            height=800,
        )
    return fig

# Callback pour mettre à jour le graphique en fonction de la période sélectionnée
@app.callback(
    Output('articles-line-chart', 'figure'),
    [Input('search-button', 'n_clicks')],
    [State('date-picker-range', 'start_date'), State('date-picker-range', 'end_date')]
)
def update_line_chart(n_clicks, start_date, end_date):
    if n_clicks > 0 and start_date and end_date:
        articles = filter_articles_by_date(start_date, end_date)
        
        # Créer un DataFrame pandas à partir des articles
        df = pd.DataFrame(articles)
        df['date_publication'] = pd.to_datetime(df['date_publication'])
        
        # Compter le nombre d'articles par date
        articles_count = df.groupby(df['date_publication'].dt.date).size().reset_index(name='count')
        
        # Créer le line chart
        fig = px.line(
            articles_count,
            x='date_publication',
            y='count',
            labels={'date_publication': 'Date de publication', 'count': "Nombre d'articles"},
            title="Évolution du nombre d'articles publiés"
        )
        return fig
    else:
        return {}

if __name__ == '__main__':
    app.run_server(debug=True)
