from fastapi import FastAPI, HTTPException, Query
import mysql.connector
from typing import List, Optional
from models import News
from datetime import datetime

# Connexion à la base de données
connection = mysql.connector.connect(
    host="xxxx",
    user="xxxx",
    password="xxxx",
    database="xxxx"
)
cursor = connection.cursor()

app = FastAPI()

# 1-Route pour rechercher des articles par mots-clés
@app.get("/articles/keywords", response_model=List[News])
async def search_articles_by_keywords(keywords: Optional[List[str]] = Query(None)):
    if keywords:
        query = "SELECT * FROM news WHERE "
        conditions = []
        for keyword in keywords:
            conditions.append(f"title LIKE '%{keyword}%' OR description LIKE '%{keyword}%'")
        query += " OR ".join(conditions)
    else:
        query = "SELECT * FROM news"
    cursor.execute(query)
    news_records = cursor.fetchall()

    news_list = []
    for record in news_records:
        news = News(
            id=record[0],
            title=record[1],
            description=record[2],
            URL_news=record[3],
            date_publication=str(record[4]),
            id_sources=record[5]
        )
        news_list.append(news)
    return news_list

# 2-Route pour rechercher des articles par catégorie
@app.get("/articles/category")
async def search_articles_by_category(category: str):
    query = """
        SELECT n.id, n.title, n.description, n.URL_news, n.date_publication, s.name AS source_name
        FROM news n
        INNER JOIN sources s ON n.id_sources = s.id_sources
        INNER JOIN news_category nc ON n.id = nc.id_news
        INNER JOIN category c ON nc.id_category = c.id_category
        WHERE c.name_categ = %s
    """
    cursor.execute(query, (category,))
    articles_records = cursor.fetchall()

    articles = []
    for record in articles_records:
        article = {
            "id": record[0],
            "title": record[1],
            "description": record[2],
            "URL_news": record[3],
            "date_publication": record[4],
            "source_name": record[5]
        }
        articles.append(article)
    
    return {"articles": articles}


# 3-Route pour rechercher des articles par source
@app.get("/articles/source/{source_id}", response_model=List[News])
async def search_articles_by_source(source_id: int, keywords: Optional[List[str]] = Query(None)):
    if keywords:
        query = "SELECT * FROM news WHERE id_sources = %s AND ("
        conditions = []
        for keyword in keywords:
            conditions.append(f"title LIKE '%{keyword}%' OR description LIKE '%{keyword}%'")
        query += " OR ".join(conditions)
        query += ")"
        cursor.execute(query, (source_id,))
    else:
        query = "SELECT * FROM news WHERE id_sources = %s"
        cursor.execute(query, (source_id,))
    news_records = cursor.fetchall()

    news_list = []
    for record in news_records:
        news = News(
            id=record[0],
            title=record[1],
            description=record[2],
            URL_news=record[3],
            date_publication=str(record[4]),
            id_sources=record[5]
        )
        news_list.append(news)
    return news_list

# 4-Route pour rechercher les articles en fonction de leur ID
@app.get("/articles/{article_id}")
async def read_article(article_id: int):
    cursor.execute("SELECT n.*, s.name FROM news n INNER JOIN sources s ON n.id_sources = s.id_sources WHERE n.id = %s", (article_id,))
    article_record = cursor.fetchone()

    if article_record is None:
        raise HTTPException(status_code=404, detail="Article non trouvé")

    cursor.execute("SELECT id_category FROM news_category WHERE id_news = %s", (article_id,))
    categories_records = cursor.fetchall()
    categories = []
    for category_record in categories_records:
        categories.append(category_record[0])

    categories_names = []
    for category_id in categories:
        cursor.execute("SELECT name_categ FROM category WHERE id_category = %s", (category_id,))
        category_name_record = cursor.fetchone()
        if category_name_record:
            categories_names.append(category_name_record[0])

    article_details = {
        "id": article_record[0],
        "title": article_record[1],
        "description": article_record[2],
        "URL_news": article_record[3],
        "date_publication": article_record[4],
        "id_sources": article_record[5],
        "categories": categories_names,
        "source": article_record[6]
    }
    return article_details

# 5-Route pour récupérer la liste des catégories disponibles
@app.get("/categories")
async def get_categories():
    # Requête SQL pour récupérer toutes les catégories de la table category
    cursor.execute("SELECT name_categ FROM category")
    categories_records = cursor.fetchall()

    # Formater les catégories en une liste simple
    categories = [record[0] for record in categories_records]
    
    return {"categories": categories}

# 6-Route pour récupérer la liste des sources disponibles
@app.get("/sources")
async def get_sources():
    # Requête SQL pour récupérer les noms des sources
    query = "SELECT id_sources, name FROM sources"
    cursor.execute(query)
    sources_records = cursor.fetchall()

    # Formater les résultats de la requête en une liste de dictionnaires représentant les sources
    sources = []
    for record in sources_records:
        source = {
            "id_sources": record[0],
            "name": record[1]
        }
        sources.append(source)
    
    return {"sources": sources}

# 7-Route pour récupérer les articles avec option de tri par date
@app.get("/articles", response_model=List[News])
async def get_articles(
    start_date: Optional[datetime] = None,
    sort_by_date: Optional[str] = None
):
    # Requête SQL de base
    query = """
        SELECT n.id, n.title, n.description, n.URL_news, n.date_publication, n.id_sources, c.name_categ
        FROM news AS n
        INNER JOIN news_category AS nc ON n.id = nc.id_news
        INNER JOIN category AS c ON nc.id_category = c.id_category
    """

    # Si une date de début est spécifiée, ajouter une condition WHERE pour filtrer les articles
    if start_date:
        query += f" WHERE n.date_publication >= '{start_date}'"

    # Si une option de tri par date est spécifiée, ajouter la clause ORDER BY
    if sort_by_date:
        query += f" ORDER BY n.date_publication {sort_by_date.upper()}"

    # Exécution de la requête SQL
    cursor.execute(query)
    news_records = cursor.fetchall()

    # Formatage des résultats en une liste d'objets News
    news_list = []
    for record in news_records:
        news = News(
            id=record[0],
            title=record[1],
            description=record[2],
            URL_news=record[3],
            date_publication=str(record[4]),  
            id_sources=record[5],
            category=record[6], 
        )
        news_list.append(news)

    return news_list


